a = 1, 2, 3, 4, 5
b = [4, 5, 6, 7, 8]
c = list(range(1,5))
d = "ABC"

a[start:stop:step]  # items start through stop-1
a[start:stop]       # items start through stop-1
a[start:]           # items start through the rest of the array
a[:stop]            # items from the beginning through stop-1
a[:]                # a copy of the whole array

::-x                # Immer Rückwärts die zahlen aufschreiben
-x:: = -x:          # Die Anzahl x von hinten löschen

print(type(a))                     # <class 'tuple'> # auch ok: tuple
print(a)                           # (1, 2, 3, 4, 5)
print(type(b))                     # <class 'list'> # auch ok: list
print(b)                           # [4, 5, 6, 7, 8]
print(b[2])                        # 6
print(b[2:2])                      # []
print(b[2:3])                      # [6]
print(b[-2])                       # 7
print(b[:-2])                      # [4, 5, 6]   Lösche die letzten beiden elemente
print(b[:2])                       # [4, 5]      Start=0 Ende=2-1
print(b[::2])                      # [4, 6, 8]   Start=0 bis Ende in 2er Schritten
print(b[::3])                      # [4, 7]      Start=0 bis Ende in 3er Schritten
print(b[::-3])                     # [8, 5]      Gehe von hinten durch die Liste mit Schrittweite 3
print(b[1:3:3])                    # [5]
print(b[-2:])                      # [7, 8]      Fange von hinten an, die letzten 2 elemente
print(b[-3:])                      # [6, 7, 8]   Fange von hinten an, die letzten 3 elemente
print(b[-1::-2])                   # [8, 6, 4]
print(b*2)                         # [4, 5, 6, 7, 8, 4, 5, 6, 7, 8]
print(c)                           # [1, 2, 3, 4]
c[1:1] = 'a'    #(keine Ausgabe)
print(c)                           # [1, 'a', 2, 3, 4]
c[3:3] = 'abc'  #(keine Ausgabe)
print(c)                           # [1, 'a', 2, 'a', 'b', 'c', 3, 4]
c[::2] = '0123' #(keine Ausgabe)
print(c)                           # ['0', 'a', '1', 'a', '2', 'c', '3', 4]
x = [1] * 5     #(keine Ausgabe)
print(x)                           # [1, 1, 1, 1, 1]

s[i:j:k]                       # Slicing: Ausschnitt von s von i bis j mit Schrittweite k; Genauer: Elemente
                               # mit Index x = i + n*k, so dass 0 <= n < (j-i/k); ; Standardwert für k ist 1
