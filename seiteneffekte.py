i = 1, 2, 3
j = i
m = [10, 20, 30]
n = m
j += 9,
print(j)                   # (1, 2, 3, 9)
m += 99,
print(n)                   # [10, 20, 30, 99]

x = 0
y = x
x+= 1
print(y)                   # 0

g = {1, 2, 3}
h = g
g |= {3, 4, 5}
print(h)                   # {1, 2, 3, 4, 5}

def f(a,b):
  a += 9,
  b += 99,
  return a,b
t = (1, 2, 3)
e = [10, 20, 30]
x,y = f(t, e)
print(t)                   # (1, 2, 3)
print(e)                   # [10, 20, 30, 99]
print(x)                   # (1, 2, 3, 9)
print(y)                   # [10, 20, 30, 99]
