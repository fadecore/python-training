'''
Gegeben seien ein Dictionary mit der Bezeichnung d und zwei Listen mit den Bezeichnungen schlüssel bzw. wert. 
Schlüssel und Wert korrespondieren positionsbezogen (wert[0] gehört zu schlüssel[0] etc.)
Bsp.
d = {'a': 1, 'b': 2}
schlüssel = ['b', 'c', 'd']
wert = [10, 11, 12]
# Der Wert zum Schlüssel 'c' wäre demnach 11.

Erstellen Sie eine Funktion schlüssel_eintragen(d, schlüssel, wert). 
Die Funktion soll die Schlüssel-Wert-Paare der beiden Listen in das Dictionary d eintragen. 
Die Eintragung eines Schlüssels soll aber nur dann erfolgen, 
wenn der Schlüssel noch nicht im Dictionary enthalten ist. 
(Standardmäßig würde die Aktualisierung eines Dictionarys bereits vor-
handene Eintragungen eines Schlüssels überschreiben.) Die Funktion soll die Anzahl der neu
eingetragenen Werte zurückgeben.
'''

d = {'a':1,'b':2}
schluessel = ['b', 'c','d']
wert = [10, 11, 12]

def schluessel_eintragen(d, schluessel, wert):
    i = 0                                           # zähler
    changes = 0                                     # Anzahl neuer Eintragungen
    while i <= len(schluessel)-1:                   # So lange i kleiner der Listenlänge ist, wichtitg: len -1, da man i mit 0 beginnt, len aber ab 1 zählt
        key = schluessel[i]
        value = wert[i]
        print(key,':', value)
        if key not in d:                            # Schauen ob in der key aus Schlüssel schon in d enthalten ist
            d[key] = value                          # Neues Wertepaar in d einfügen
            changes +=1                             # Anzahl der Neueintragungen in d hochzählen
        i += 1                                      # Schleifenzähler für die Schlüssel-Listenlänge
    return(changes)                                 # Nach der Schleife Anzahl der Änderungen zurückgeben
changes = schluessel_eintragen(d, schluessel, wert) # Funktion aufrufen um die Anzahl der Änderungen zu erhalten
print(d)                                            # Ausgabe von d um zu prüfen ob die neuen Wertepaare enthalten sind
print("Changes:", changes)                          # Ausgabe der Anzahl de Änderungen
