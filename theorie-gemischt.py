print( 13 // 6 )               # 2  abgerundeter Quotient - Division ohne Rest!
print( 12 / 6 )                # 2.0 bei Division wird Ergebnis float
print( 13 % 6 )                # 1  Modulo "Rest"
print( 13 << 2 )               # 52 Bitverschiebung nach links
print(list(range(3)))          # [0, 1, 2]
print(list(range(0,10,2)))     # [0, 2, 4, 6, 8]
print(list(range(-4)))         # []
print('print("print")')        # print("print")
print( 3 * '7')                # 777
print([x*100 for x in
  range(-3,3) if x > 0])       #[100, 200]

i = 0
while True:
  i += 1
  if i == 2:
    continue
  print(i)
  if i == 4:                   # 1
    break                      # 3
else:                          # 4
  print("Ende")

c = 2 + 8j;
print(c)                       # (2+8j) type=complex KEIN Tuple
  

s.index(x[,i[,j]])             # Index des ersten Auftretens von x in s (ab Index i und vor j);
                               # ValueError falls x nicht in s vorkommt!
s = "Zeichenkette"
s.index('e')                   # 1
s.index('e', 2)                # 5

s[i]                           # Element an Stelle i von s (Start bei 0)
                               # Wenn i < 0: Start vom Ende (len(s)+i)

s[i:j]                         # Slicing: Ausschnitt (slice) von s von i bis j; Genauer: Elemente mit Index
                               # k für die gilt: i <= k < j; Standardwert für i ist 0

s[i:j:k]                       # Slicing: Ausschnitt von s von i bis j mit Schrittweite k; Genauer: Elemente
                               # mit Index x = i + n*k, so dass 0 <= n < (j-i/k); ; Standardwert für k ist 1
