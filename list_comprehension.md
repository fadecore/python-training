# Allgemeine Form
* [f(x) for x in S if P(x)]  
    * erzeugt eine Liste, basierend auf den Werten der Sequenz S  
    * gefiltert durch das Prädikat P und angewendet durch die Funktion f  

# Beispiele:
``` python
i = 1,2,3,4,5,6,7,8

[x for x in i if x%2==0]          # [2, 4, 6, 8]

[x**2 for x in i if x%2==0]       # [4, 16, 36, 64]

[x**(1/2) for x in i if x>0]      # [3.0, 6.0, 8.0]

a = 1,2,3
b = 11,22,33
[x*y for x in a if x%2==0 for y in b if y*2==1]    # [22, 66]

a = 1,2,3
b = 10,20,30
[x*y for x in a for y in b]                        # [10, 20, 30, 20, 40, 60, 30, 60, 90]



```
