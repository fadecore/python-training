'''
Gegeben sei eine Liste mit paarweise verschiedenen Zahlen. Erstellen Sie eine Funktion f(liste). 
Die Funktion soll zunächst prüfen, ob die übergebene liste nur Zahlen enthält (int oder float) 
und ob die Zahlen tatsächlich paarweise verschieden sind. Falls die Bedingungen erfüllt sind, 
soll die Funktion auf der Grundlage der liste ein Dictionary zurückliefert, das als Schlüssel
die Zahlen aus liste und als Wert die Quadratzahl (Zahl*Zahl) des jeweiligen Schlüssels enthält.
Falls die Bedingung nicht erfüllt ist, soll eine Fehlermeldung auf dem Bildschirm ausgegeben
und None zurückgegeben werden.
Bsp.:
z = [33, 20, 6, 8, 18]
d = f(z)
--> d = {33: 1089, 20: 400, 6: 36, 8: 64, 18: 324}
z = [33, 33, 6, 8, 18] oder z = [33, 'a', 6, 8, 18]
d = f(z)
--> d = None
'''

def f(liste):
    i = 0
    d = {}
    for x in liste:                             # Schleifen um zu prüfen ob alles int oder float ist
        if type(x) == float or type(x) == int:  # wenn int oder float Ausgabe
            print("int oder float: ", x) 
            i += 1                              # Schleifenzähle um nächstes Element der liste u prüfen
        else:                                   # Wenn es kein int oder float ist Ausgabe 
            print("Nicht int und float: ", x)       
            i += 1
            return(None)                        # Funktion beenden und "None" zurückliefern
    if len(set(liste)) == len(liste):           # prüfen ob doppelte Wert enthalten sind
        print("Paarweise verschieden")
    else:
        print("Nicht paarweise verschieden")
        return(None)                            # Bei Fehler(doppelten Werten) Funktion beenden und "None" zurückgeben
    for x in liste:                             # Wenn keine Prüfung zu Funktionsabbruch geführt hat 
        d[x] = x*2                              # quadrieren und dem dict d hinzufügen {'x':x*2}
    return(d)                                   # das befüllte dict zurückgeben

z = [33, 20, 6, 8, 18]                          
print("normaler Durchlauf: ")
d = f(z)
print(d)

x = [33, 33, 20, 6, 8, 18]
print("Doppeltes Element: ")
d = f(x)
print(d)

y = [33, "a", 20, 6, 8, 18]
print("Buchstabe: ")
d = f(y)
print(d)
