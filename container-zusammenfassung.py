#####
#
# Liste
#
#####
# Erzeugung einer leeren oder vorbelegte Liste
liste_1 = []                 # oder liste_1 = list()
liste_2 = [1,2,3]            # oder liste_2 = list(range(1,4)
# Lesender / verändernder Zugriff auf ein Objekt
liste_2[0]                   # --> 1
liste_2[0] = 0               # --> [0, 2, 3]
# Hinzufügen von Objekten
liste_2 += [4, 5]            # oder liste_2.extend([4, 5]) --> [0, 2, 3, 4, 5]
# Hinzufügen von Objekten an bestimmter Stelle (ist neu hinzugekommen, nachfolgende beziehn das nicht ein)
#liste_2.insert(1) = 1       # [0, 1, 2, 3, 4, 5]
# Entfernen von Objekten, die letzten beiden
del liste_2 [-2:]            # --> [0, 2, 3]
# Suche nach Objekten
2 in liste_2                 # --> True
liste_2.index(3)             # --> 2
liste_2.count(2)             # --> 1
# Iteration
for element in liste_2:
  print(element)             # --> 0 2 3
# Zeige das letze Element einer Liste
liste_2[len(liste_2)-1]     # 3
  
#####
#
# Tupel
#
#####
# Erzeugung eines leeren oder vorbelegte Tupels
tupel_1 = ()                 # oder tuple_1 = tuple()
tuple_2 = (1,2,3)            # oder tuple = 1,2,3 / tuple_2 = tuple(range(1,4)
# Lesender Zugriff auf ein Objekt (verändernder Zugriff nicht möglich)
tuple_2[0]                   # --> 1
# Hinzufügen von Objekten (hierbei wird ein neues Tupel erzeugt!)
tuple_2 += (4, 5)            # oder tuple += 4, 5
# Entfernen von Objekten
# Nicht möglich!
# Suche nach Objekten
2 in tuple_2                 # --> True
tuple_2.index(2)             # --> 1
tuple_2.count(2)             # --> 1
# Iteration
for element in tuple_2:
  print(element)             # --> 1 2 3
 

#####
#
# Mengen
#
#####
# Erzeugung einer leeren oder vorbelegten Menge
set_1 = set()
set_2 = {1,2,3}             # oder set_2 = set(range(1,4))
# Lesender Zugriff auf ein Objekt nicht direkt möglich (s. Suche/Iteration)
# Hinzufügen von Objekten (nur bei set, nicht bei frozenset)
set_2.add(4)                # oder set_2 |= {4} --> {1, 2, 3, 4}
set_2.update({5, 6})        # oder set |= {5, 6} --> {1, 2, 3, 4, 5, 6}
# Entfernen von Objekten
set_2.remove(6)             # oder set_2.discard(6) --> {1, 2, 3, 4, 5}
set_2 -= {4, 5}             # --> {1, 2, 3}
# Suche nach Objekten
2 in set_2                  # --> True
# Iteration
for element in set_2:
  print(element)            # --> 1 2 3

  
#####
#
# Zuordungen
#
#####
# Erzeugung einer leeren oder vorbelegten Zuordnung
dict_1 = {}                       # oder set_1 = dict()
dict_2 = {'eins': 1, 'zwei': 2}   # oder dict_2 = dict(eins=1, zwei=2)
# Lesender /verändernder Zugriff auf ein Objekt
dict_2['zwei']                    # --> 2
dict_2['zwei'] = 22               # --> {'eins': 1, 'zwei': 22, 'drei': 3}
# Hinzufügen von Objekten
dict_2['drei'] = 4                # --> {'eins': 1, 'zwei': 22, 'drei': 3}
dict_2.update({'vier': 4, 'fünf': 5})
                                  # --> {'eins': 1, 'zwei': 22, 'drei': 3, 'vier': 4, 'fünf': 5}
# Entfernen von Objekten
del dict_2['fünf']                # --> {'eins': 1, 'zwei': 22, 'drei': 3, 'vier': 4}
# Suche nach Objekten
'zwei' in dict_2                  # --> True
# Iteration
for schlüssel in dict_2:
  print(schlüssel)                # --> eins zwei drei vier
# Ausgabe der keys
dict_2.keys()                     # --> dict_keys(['eins', 'zwei', 'drei', 'vier'])
# Keys in einer Liste
list(dict_2.keys())               # --> ['eins', 'zwei', 'drei', 'vier']             
# Ausgabe der Values
dict_2.values()                   # --> dict_values(['1', '22', '3', '4'])
# Values in einer Liste
list(dict_2.values())             # --> ['1', '22', '3', '4']
# Schlüssel/Wertpaar ausgeben
dict_2.items()                    # --> dict_items([('eins', 1), ('zwei', 22), ('drei', 3), ('vier', 4)])  

