# python-training 

## Typisierung
* Python hat **dynamische** Typisierung (Nutzung von Duck Typing)
* Python hat **starke** Typisierung - nur typkonforme Zugriffe a=5 b='2' c=a+b bringt fehler
* Variablen referenzieren Instanzen
* ID und Typ einer Instanz sind nicht änderbar
* Wert einer Instanz ist nur bei ‚mutable‘ Objekten änderbar
* bei Verwendung von ‚mutable‘ Objekten kann es zu Seiteneffekten kommen
* Referenzen auf Instanzen können über del gelöscht werden  
(Instanz wird gelöscht, wenn keine Referenz mehr auf sie zeigt)  


Seite 158 fortsetzen
  



if/elif/else: vergleichbar mit Kontrollstruktur
switch/case in anderen Sprachen.

Funktionsname([Parameter])  
Objekt.Methodenname([Parameter])  

``` python  
Text = "Hallo Welt!"  
liste = [1,3,9,2]  
print(Text)  
len("FOM")  
max(liste)  
liste.sort()  
liste.count("l")  
```

## Immutable Datentypen
* Numerische Objekte
* Strings
* Tuples
* Bytes
* Frozen Sets

## Mutable Datentypen
* List
* Byte Array
* Set
* Dictionary
