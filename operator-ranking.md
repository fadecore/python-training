## Operatorranking
```python
Prio   Operator              Beschreibung
1. { }                   # Anzeige Dictionary /Set
2. [ ]                   # Anzeige Liste
3. ( )                   # Klammerung, Anz. Tupel
4. Objekt.Attribut       # Referenzierung
5. Funktion(Argumente)   # Funktionsaufruf
6. x[Index:Index]        # Slice
7. x[Index]              # Indizierung
8. **                    # Potenzierung
9. ~x                    # bitweises Nicht
10. +x, -x               # pos./neg. Vorzeichen
11. *, /, //, %          # Multipli., Div., Div.rest
12. +, -                 # Addition, Subtraktion
13. <<, >>               # bitweise Verschiebung
14. &                    # bitweises Und
15. ^                    # Bitweises Exkl.-Oder
16. |                    # bitweises Oder
17. <, <=, >, >=, !=, == # Vergleichsoperatoren
18. is, is not           # Test auf Identität
19. in,not in            # Test auf Mitgliedschaft
20. not x                # logisches Nicht
21. and                  # logisches Und
22. or                   # logisches Oder
23. lambda               # Lambda-Ausdruck
```

## Bit-Operatoren 
```python
x << y
    # Returns x with the bits shifted to the left by y places (and new bits on the right-hand-side are zeros). This is the same as multiplying x by 2**y.
x >> y
    # Returns x with the bits shifted to the right by y places. This is the same as //'ing x by 2**y.
x & y
    # Does a "bitwise and". Each bit of the output is 1 if the corresponding bit of x AND of y is 1, otherwise it's 0.
x | y
    # Does a "bitwise or". Each bit of the output is 0 if the corresponding bit of x AND of y is 0, otherwise it's 1.
~ x
    # Returns the complement of x - the number you get by switching each 1 for a 0 and each 0 for a 1. This is the same as -x - 1.
x ^ y
    # Does a "bitwise exclusive or". Each bit of the output is the same as the corresponding bit in x if that bit in y is 0, and it's the complement of the bit in x if that bit in y is 1.

```
