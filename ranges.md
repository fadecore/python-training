# Tupel-Erzeugung

``` python
>>> list(range(5))
[0, 1, 2, 3, 4]
>>> list(range(10,15))
[10, 11, 12, 13, 14]
>>> list(range(1,11,2))
[1, 3, 5, 7, 9]
>>> list(range(0,-5,-1))
[0, -1, -2, -3, -4]
>>> list(range(0,-10))
[]
>>> list(range(0))
[]
# ABC-Funktionen
>>> r = range(0,10,2); r
range(0,10,2)
>>> list(r)
[0, 2, 4, 8]
>>> 5 in r
False
>>> r.index(2)
1
>>> r[2]
4
>>> r[0:3]
range(0, 6, 2)

``` 
